# README

Trocar Dice

```powershell
npm install trocar-dice-js
```

Usage:

```js
import dice from 'trocar-dice-js';

console.log(dice.d20());

console.log(dice.dice(4,6));

```

Possible outputs:

```

5
[2,5]

```

Functions:

d2,d3,d4,d6,d8,d10,d12,d20,d100

These take no parameters and return a number from 1 to and including the number after d in the function name. These call the function "die" which takes a parameter of number of sides.

dice(sides, count)

This returns an array of roll results.



